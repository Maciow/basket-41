function resetPassrowd(el) {
    var userId = el.getAttribute('value');
    var response = confirm("Czy na pewno chcesz zresetować hasło dla użytkownika o id="+userId+"?");
    if (response===true){
        var coreLink = '/admin/users/reset/password/';
        window.location.href=coreLink+userId;
    }
}