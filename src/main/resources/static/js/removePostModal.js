function confirmRemovePost(el) {
    var response = confirm("Czy na pewno chcesz usunąć post?");
    if (response === true){
        var coreLink = '/games/remove/';
        var gameId = el.getAttribute('value');
        window.location.href=coreLink+gameId;
    }
}