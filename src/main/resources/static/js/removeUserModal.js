function confirmRemoveUser(el) {
    var response = confirm("Czy na pewno chcesz usunąć użytkownika?")
    if (response === true) {
        var coreLink = '/admin/users/remove/';
        var userId = el.getAttribute('value');
        window.location.href=coreLink+userId;
    }
}