package pl.basket.wrapper;

import pl.basket.model.UserState;

import java.util.Objects;

public class UserStateWrapper {

    private UserState userState;

    public UserStateWrapper() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserStateWrapper that = (UserStateWrapper) o;
        return userState == that.userState;
    }

    @Override
    public int hashCode() {
        return Objects.hash(userState);
    }

    public UserState getUserState() {
        return userState;
    }

    public void setUserState(UserState userState) {
        this.userState = userState;
    }
}
