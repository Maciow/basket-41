package pl.basket.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.basket.model.Meeting;
import pl.basket.model.User;
import pl.basket.model.Vote;
import pl.basket.model.VoteType;
import pl.basket.repository.MeetingRepository;
import pl.basket.repository.UserRepository;
import pl.basket.repository.VoteRepository;
import pl.basket.utils.UserUtils;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.List;

@Service
public class VoteService {

    private MeetingRepository meetingRepository;
    private VoteRepository voteRepository;
    private UserRepository userRepository;

    @Autowired
    public void setUserRepository(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Autowired
    public void setMeetingRepository(MeetingRepository meetingRepository) {
        this.meetingRepository = meetingRepository;
    }

    @Autowired
    public void setVoteRepository(VoteRepository voteRepository) {
        this.voteRepository = voteRepository;
    }

    public void addVote(Long id, VoteType type){
        Meeting meeting = meetingRepository.findById(id).get();
        String nickname = UserUtils.getLoggedUserNickname();
        User user = userRepository.findByNickname(nickname);
        Vote vote = voteRepository.findByMeetingAndUser(meeting,user);
        if (vote==null){
            addNewVote(meeting,user,type);
        } else {
            VoteType oldType = vote.getVoteType();
            if (!oldType.equals(type)||oldType!=type){
                Long idVote = vote.getId();
                updateVotes(oldType,meeting,false);
                updateVotes(type, meeting,true);
                voteRepository.updateVoteType(type,idVote);
            }
        }
    }

    private void updateVotes(VoteType type, Meeting meeting, boolean test) {
        switch (type){
            case PLUS:
                changePlusVote(meeting,test);
                break;
            case MAYBE:
                changeMaybeVote(meeting,test);
                break;
            case MINUS:
                changeMinusVote(meeting,test);
                break;
        }
    }

    private void addNewVote(Meeting meeting, User user, VoteType type){
        Vote vote = new Vote();
        Timestamp timestamp = Timestamp.from(Instant.now());
        vote.setMeeting(meeting);
        vote.setTimestamp(timestamp);
        vote.setUser(user);
        vote.setVoteType(type);
        updateVotes(type, meeting,true);
        voteRepository.save(vote);
    }

    private void changePlusVote(Meeting meeting, boolean test){
        int plusVotes = meeting.getPlus();
        int incrementValue = plusVotes+1;
        int decrementValue = plusVotes-1;
        Long meetingId = meeting.getId();
        if (test) {
            meetingRepository.updatePlusVotes(incrementValue, meetingId);
        } else meetingRepository.updatePlusVotes(decrementValue,meetingId);
    }

    private void changeMaybeVote(Meeting meeting, boolean test){
        int maybeVotes = meeting.getMaybe();
        int incrementValue = maybeVotes+1;
        int decrementValue = maybeVotes-1;
        Long meetingId = meeting.getId();
        if (test) {
            meetingRepository.updateMaybeVotes(incrementValue, meetingId);
        } else meetingRepository.updateMaybeVotes(decrementValue,meetingId);
    }

    private void changeMinusVote(Meeting meeting, boolean test){
        int minusVotes = meeting.getMinus();
        int incrementValue = minusVotes+1;
        int decrementValue = minusVotes-1;
        Long meetingId = meeting.getId();
        if (test) {
            meetingRepository.updateMinusVotes(incrementValue, meetingId);
        } else meetingRepository.updateMinusVotes(decrementValue,meetingId);
    }

    public void deleteVotesFromMeeting(Meeting meeting){
        voteRepository.deleteVotesFromMeeting(meeting);
    }

    public List<Vote> findByMeeting(Meeting meeting){
        return voteRepository.findByMeeting(meeting);
    }
}
