package pl.basket.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.basket.model.Comment;
import pl.basket.model.Meeting;
import pl.basket.model.User;
import pl.basket.repository.CommentRepository;
import pl.basket.repository.MeetingRepository;
import pl.basket.repository.UserRepository;
import pl.basket.utils.UserUtils;

import java.sql.Timestamp;
import java.time.Instant;

@Service
public class CommentService {

    private MeetingRepository meetingRepository;
    private UserRepository userRepository;
    private CommentRepository commentRepository;

    @Autowired
    public void setCommentRepository(CommentRepository commentRepository) {
        this.commentRepository = commentRepository;
    }

    @Autowired
    public void setUserRepository(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Autowired
    public void setMeetingRepository(MeetingRepository meetingRepository) {
        this.meetingRepository = meetingRepository;
    }

    public void addCommentWithDetails(Comment comment, Long meetingId){
        String nickname = UserUtils.getLoggedUserNickname();
        User user = userRepository.findByNickname(nickname);
        Timestamp timestamp = Timestamp.from(Instant.now());
        Meeting meeting = meetingRepository.findById(meetingId).get();
        comment.setUser(user);
        comment.setTimestamp(timestamp);
        comment.setMeeting(meeting);
        Comment newComment = new Comment(comment);
        commentRepository.save(newComment);
    }
}
