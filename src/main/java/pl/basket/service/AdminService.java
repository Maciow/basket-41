package pl.basket.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import pl.basket.model.User;
import pl.basket.repository.AdminRepository;
import pl.basket.repository.CommentRepository;
import pl.basket.repository.MeetingRepository;
import pl.basket.repository.VoteRepository;

@Service
public class AdminService {

    private static int ELEMENTS = 10;

    private AdminRepository adminRepository;
    private VoteRepository voteRepository;
    private MeetingRepository meetingRepository;
    private CommentRepository commentRepository;
    private PasswordEncoder passwordEncoder;

    @Autowired
    public AdminService(PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }

    @Autowired
    public void setCommentRepository(CommentRepository commentRepository) {
        this.commentRepository = commentRepository;
    }

    @Autowired
    public void setMeetingRepository(MeetingRepository meetingRepository) {
        this.meetingRepository = meetingRepository;
    }

    @Autowired
    public void setVoteRepository(VoteRepository voteRepository) {
        this.voteRepository = voteRepository;
    }

    @Autowired
    public void setAdminRepository(AdminRepository adminRepository) {
        this.adminRepository = adminRepository;
    }

    public Page<User> findAllUsersPageable(int page){
        return adminRepository.findAll(PageRequest.of(page,ELEMENTS));
    }

    public Page<User> findAllSearchUser(String searchWord, int page){
        return adminRepository.findAllSearch(searchWord, PageRequest.of(page,ELEMENTS));
    }

    public void resetPassword(Long userId){
        String newPassword = "1111";
        String passwordHash = passwordEncoder.encode(newPassword);
        adminRepository.resetPassword(passwordHash,userId);
    }

    public void deleteUserById(Long userId){
        if (userId!=1L) {
            voteRepository.deleteAllByUserId(userId);
            meetingRepository.deleteAllByUserId(userId);
            commentRepository.deleteAllByUserId(userId);
            adminRepository.deleteById(userId);
        }
    }
}
