package pl.basket.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import pl.basket.model.Comment;
import pl.basket.model.Meeting;
import pl.basket.model.User;
import pl.basket.model.VoteType;
import pl.basket.repository.MeetingRepository;
import pl.basket.repository.UserRepository;
import pl.basket.utils.UserUtils;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.List;

@Service
public class MeetingService {

    private static int ELEMENTS = 5;

    private MeetingRepository meetingRepository;
    private UserRepository userRepository;
    private VoteService voteService;

    @Autowired
    public void setVoteService(VoteService voteService) {
        this.voteService = voteService;
    }

    @Autowired
    public void setUserRepository(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Autowired
    public void setMeetingRepository(MeetingRepository meetingRepository) {
        this.meetingRepository = meetingRepository;
    }

    public void addMeetingWithDetails(Meeting meeting){
        Timestamp timestamp = Timestamp.from(Instant.now());
        String foundNick = UserUtils.getLoggedUserNickname();
        User user = userRepository.findByNickname(foundNick);
        meeting.setUser(user);
        meeting.setTimestamp(timestamp);
        meetingRepository.save(meeting);
        Long meetingId = meeting.getId();
        voteService.addVote(meetingId,VoteType.PLUS);
    }

    public void removeMeeting(Meeting meeting){
        voteService.deleteVotesFromMeeting(meeting);
        meetingRepository.delete(meeting);
    }

    public Page<Meeting> findAllPageable(int page){
        Page<Meeting> pages = meetingRepository.findAll(PageRequest.of(page,ELEMENTS));
        return pages;
    }

    public Meeting findById(Long id){
        return meetingRepository.findById(id).get();
    }
}
