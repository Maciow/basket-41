package pl.basket.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import pl.basket.model.User;
import pl.basket.model.UserRole;
import pl.basket.model.UserState;
import pl.basket.repository.UserRepository;
import pl.basket.repository.UserRoleRepository;
import pl.basket.utils.UserUtils;

@Service
public class UserService {
    private final static String DEFAULT_ROLE = "ROLE_USER";
    private final static UserState DEFAULT_STATE = UserState.NO;
    private UserRepository userRepository;
    private UserRoleRepository roleRepository;
    private PasswordEncoder passwordEncoder;

    @Autowired
    public UserService(PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }

    @Autowired
    public void setUserRepository(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Autowired
    public void setRoleRepository(UserRoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }

    public void addWithDefaultRole(User user){
        UserRole defaultRole = roleRepository.findByRole(DEFAULT_ROLE);
        user.getRoles().add(defaultRole);
        String passwordHash = passwordEncoder.encode(user.getPassword());
        user.setPassword(passwordHash);
        user.setState(DEFAULT_STATE);
        userRepository.save(user);
    }

    public void updateUserRole(Long roleId, Long userId){
        if (roleId!=null&&userId!=null) {
            userRepository.updateUserRole(roleId, userId);
        }
    }

    public void updateUserState(UserState userState, Long userId){
        if (userState!=null&&userId!=null){
            userRepository.updateUserState(userState,userId);
        }
    }

    public void updateNickname(String newNick){
        String loggedUser = UserUtils.getLoggedUserNickname();
        userRepository.updateNickname(newNick,loggedUser);
    }

    public void updateEmail(String newEmail){
        String loggedUser = UserUtils.getLoggedUserNickname();
        userRepository.updateEmail(newEmail,loggedUser);
    }

    public void updatePassword(String newPassword){
        String loggedUser = UserUtils.getLoggedUserNickname();
        String passwordHash = passwordEncoder.encode(newPassword);
        userRepository.updatePassword(passwordHash,loggedUser);
    }

    public User findByEmail(String email){
        return userRepository.findByEmail(email);
    }

    public User findByNick(String nickname){
        return userRepository.findByNickname(nickname);
    }

    public User findById(Long id){
        return userRepository.findById(id).get();
    }
}
