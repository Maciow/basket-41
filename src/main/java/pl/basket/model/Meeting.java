package pl.basket.model;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.io.Serializable;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.*;

@Entity
public class Meeting implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @NotEmpty(message = "{pl.basket.model.Meeting.description.NotEmpty}")
    private String description;
    private Timestamp timestamp;
    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.PERSIST)
    private User user;
    @OneToMany(fetch = FetchType.EAGER, cascade = {CascadeType.PERSIST, CascadeType.REMOVE}, mappedBy = "meeting")
    private List<Comment> comments = new ArrayList<>();
    private int plus;
    private int maybe;
    private int minus;

    public Meeting() {}

    @Override
    public String toString() {
        return "Meeting{" +
                "id=" + id +
                ", description='" + description + '\'' +
                ", timestamp=" + timestamp +
                ", user=" + user +
                ", plus=" + plus +
                ", maybe=" + maybe +
                ", minus=" + minus +
                '}';
    }

    public static class MeetingTimestampComparator implements Comparator<Meeting>{
        @Override
        public int compare(Meeting m1, Meeting m2) {
            return -(m1.getTimestamp().compareTo(m2.getTimestamp()));
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Meeting meeting = (Meeting) o;
        return plus == meeting.plus &&
                maybe == meeting.maybe &&
                minus == meeting.minus &&
                Objects.equals(id, meeting.id) &&
                Objects.equals(description, meeting.description) &&
                Objects.equals(timestamp, meeting.timestamp) &&
                Objects.equals(user, meeting.user) &&
                Objects.equals(comments, meeting.comments);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, description, timestamp, user, comments, plus, maybe, minus);
    }

    public List<Comment> getComments() {
        return comments;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }

    public int getPlus() {
        return plus;
    }

    public void setPlus(int plus) {
        this.plus = plus;
    }

    public int getMaybe() {
        return maybe;
    }

    public void setMaybe(int maybe) {
        this.maybe = maybe;
    }

    public int getMinus() {
        return minus;
    }

    public void setMinus(int minus) {
        this.minus = minus;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }
}