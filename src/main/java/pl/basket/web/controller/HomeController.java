package pl.basket.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.basket.utils.UserUtils;

@Controller
public class HomeController {

    @RequestMapping("/")
    public String home(){
        String nickname = UserUtils.getLoggedUserNickname();
        if (!nickname.equals("anonymousUser")){
            return "redirect:/games/1";
        } else return "index";
    }

    @GetMapping("/loginform")
    public String loginForm(){
        String nickname = UserUtils.getLoggedUserNickname();
        if (nickname.equals("anonymousUser")) {
            return "loginForm";
        } else return "redirect:/games/1";
    }

}
