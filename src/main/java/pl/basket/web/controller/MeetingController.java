package pl.basket.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import pl.basket.model.*;
import pl.basket.service.MeetingService;
import pl.basket.service.VoteService;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@Controller
public class MeetingController {

    private MeetingService meetingService;
    private VoteService voteService;

    @Autowired
    public MeetingController(MeetingService meetingService, VoteService voteService) {
        this.meetingService = meetingService;
        this.voteService = voteService;
    }

    @GetMapping("/meeting")
    public String writeMeeting(Model model){
        model.addAttribute("meeting",new Meeting());
        return "meetingForm";
    }

    @PostMapping("/meeting")
    public String addMeeting(@ModelAttribute @Valid Meeting meeting, BindingResult result){
        if (result.hasErrors()){
            return "meetingForm";
        } else {
            meetingService.addMeetingWithDetails(meeting);
            return "redirect:/games/1";
        }
    }

    @GetMapping("/games/{page}")
    public String listGames(@PathVariable int page, Model model){
        Page<Meeting> pages = meetingService.findAllPageable(page-1);
        int totalPages = pages.getTotalPages();
        int currentPage = pages.getNumber();
        List<Meeting> meetings = pages.getContent();
        model.addAttribute("totalPages",totalPages);
        model.addAttribute("currentPage",currentPage+1);
        model.addAttribute("comment", new Comment());
        model.addAttribute("meetings",meetings);
        return "timelineForm";
    }

    @GetMapping("/games/remove/{meeting}")
    @Secured(value = {"ROLE_ADMIN"})
    public String removeMeeting(@PathVariable Meeting meeting){
        meetingService.removeMeeting(meeting);
        return "redirect:/games/1";
    }

    @GetMapping("/meeting/{id}/details")
    public String getDetails(@PathVariable Long id, Model model){
        Meeting meeting = meetingService.findById(id);
        List<Vote> votes = voteService.findByMeeting(meeting);
        List<String> nicknamesPlus = new ArrayList<>();
        List<String> nicknamesMaybe = new ArrayList<>();
        List<String> nicknamesMinus = new ArrayList<>();
        for (Vote vote:votes){
            switch (vote.getVoteType()){
                case PLUS:
                    nicknamesPlus.add(vote.getUser().getNickname());
                    break;
                case MAYBE:
                    nicknamesMaybe.add(vote.getUser().getNickname());
                    break;
                case MINUS:
                    nicknamesMinus.add(vote.getUser().getNickname());
                    break;
            }
        }
        model.addAttribute("plusUsers",nicknamesPlus);
        model.addAttribute("maybeUsers",nicknamesMaybe);
        model.addAttribute("minusUsers",nicknamesMinus);
        model.addAttribute("meeting",meeting);
        model.addAttribute("comment",new Comment());
        return "detailsForm";
    }
}
