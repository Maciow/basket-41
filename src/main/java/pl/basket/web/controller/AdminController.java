package pl.basket.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import pl.basket.model.User;
import pl.basket.model.UserRole;
import pl.basket.model.UserState;
import pl.basket.service.AdminService;
import pl.basket.service.UserRoleService;
import pl.basket.service.UserService;
import pl.basket.wrapper.UserStateWrapper;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Controller
public class AdminController {

    private AdminService adminService;
    private UserService userService;
    private UserRoleService userRoleService;

    @Autowired
    public AdminController(AdminService adminService, UserService userService, UserRoleService userRoleService){
        this.adminService = adminService;
        this.userService = userService;
        this.userRoleService = userRoleService;
    }

    @GetMapping("/admin/users/{page}")
    @Secured(value = {"ROLE_ADMIN"})
    public String getUserManagement(@PathVariable int page, Model model){
        Page<User> pages = adminService.findAllUsersPageable(page-1);
        int totalPages = pages.getTotalPages();
        int currentPage = pages.getNumber();
        List<User> users = pages.getContent();
        model.addAttribute("totalPages",totalPages);
        model.addAttribute("currentPage",currentPage+1);
        model.addAttribute("users",users);
        return "admin/usersManager";
    }

    @GetMapping("/admin/users/search/{searchWord}/{page}")
    @Secured(value = {"ROLE_ADMIN"})
    public String getSearchUserManagement(@PathVariable String searchWord,
                                          @PathVariable int page, Model model){
        Page<User> pages = adminService.findAllSearchUser(searchWord,page-1);
        int totalPages = pages.getTotalPages();
        int currentPage = pages.getNumber();
        List<User> users = pages.getContent();
        model.addAttribute("totalPages",totalPages);
        model.addAttribute("currentPage",currentPage+1);
        model.addAttribute("users",users);
        model.addAttribute("searchWord",searchWord);
        return "admin/userSearchManager";
    }

    @GetMapping("/admin/users/edit/{id}")
    @Secured(value = {"ROLE_ADMIN"})
    public String editUser(@PathVariable Long id, Model model){
        User user = userService.findById(id);
        List<UserRole> userRoles = userRoleService.getUserRoleList();
        model.addAttribute("user",user);
        model.addAttribute("userRoles",userRoles);
        model.addAttribute("singleRole",new UserRole());
        model.addAttribute("singleState", new UserStateWrapper());
        model.addAttribute("statesList", new ArrayList<>(Arrays.asList(UserState.values())));
        return "admin/editUser";
    }

    @GetMapping("/admin/users/remove/{id}")
    @Secured(value = {"ROLE_ADMIN"})
    public String removeUser(@PathVariable Long id){
        adminService.deleteUserById(id);
        return "redirect:/admin/users/1";
    }

    @PostMapping("/admin/users/edit/{userId}/role/save")
    @Secured(value = {"ROLE_ADMIN"})
    public String saveUserRoleChanges(@ModelAttribute UserRole singleRole,
                                      @PathVariable Long userId){
        Long roleId = singleRole.getId();
        userService.updateUserRole(roleId,userId);
        return "redirect:/admin/users/1";
    }

    @PostMapping("/admin/users/edit/{userId}/state/save")
    @Secured(value = {"ROLE_ADMIN"})
    public String saveUserStateChanges(@ModelAttribute UserStateWrapper stateWrapper,
                                       @PathVariable Long userId){
        UserState userState = stateWrapper.getUserState();
        userService.updateUserState(userState,userId);
        return "redirect:/admin/users/1";
    }

    @GetMapping("/admin/users/reset/password/{userId}")
    @Secured(value = {"ROLE_ADMIN"})
    public String resetUserPassword(@PathVariable Long userId){
        adminService.resetPassword(userId);
        return "redirect:/admin/users/1";
    }
}
