package pl.basket.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import pl.basket.model.Comment;
import pl.basket.service.CommentService;

import javax.validation.Valid;

@Controller
public class CommentController {

    private CommentService commentService;

    @Autowired
    public void setCommentService(CommentService commentService) {
        this.commentService = commentService;
    }

    @PostMapping("/comment/{id}/save")
    public String saveComment(@ModelAttribute @Valid Comment comment,
                              @PathVariable Long id,
                              BindingResult result){
        //sypie błędem
        if (result.hasErrors()){
            return "redirect:/error";
        } else {
            commentService.addCommentWithDetails(comment,id);
            return "redirect:/games/1";
        }
    }
}
