package pl.basket.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import pl.basket.model.User;
import pl.basket.captcha.service.ICaptchaService;
import pl.basket.service.UserService;
import pl.basket.validator.UserEditValidator;
import pl.basket.validator.UserRegistrationValidator;
import pl.basket.wrapper.UserEditWrapper;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@Controller
public class UserController {

    private UserService userService;
    private ICaptchaService captchaService;

    @Autowired
    public UserController(UserService userService, ICaptchaService captchaService) {
        this.userService = userService;
        this.captchaService = captchaService;
    }

    @GetMapping("/register")
    public String register(Model model){
        model.addAttribute("user",new User());
        return "registerForm";
    }

    @PostMapping("/register")
    public String addUser(@Valid @ModelAttribute User user, BindingResult result, HttpServletRequest request){
        String response  = request.getParameter("g-recaptcha-response");
        captchaService.processResponse(response,result);

        User userEmailExist = userService.findByEmail(user.getEmail());
        User userNickExist = userService.findByNick(user.getNickname());

        new UserRegistrationValidator().validateEmailExist(userEmailExist,result);
        new UserRegistrationValidator().validateNicknameExist(userNickExist,result);
        new UserRegistrationValidator().validateConfirmPassword(user,result);

        if (result.hasErrors()){
            return "registerForm";
        } else {
            userService.addWithDefaultRole(user);
            return "registerSuccess";
        }
    }

    @GetMapping("/edit/profile")
    public String editProfile(Model model){
        model.addAttribute("userWrapper", new UserEditWrapper());
        return "editProfile";
    }

    @PostMapping("/edit/profile/save")
    public String saveProfileEdits(@Valid @ModelAttribute("userWrapper") UserEditWrapper userWrapper, BindingResult result){
        if (userWrapper.getNickname()!=null && !userWrapper.getNickname().equals("")){
            User userNickExist = userService.findByNick(userWrapper.getNickname());
            if (userNickExist!=null) {
                UserEditWrapper copyWrapper = new UserEditWrapper(userNickExist);
                new UserEditValidator().validateNicknameExist(copyWrapper, result);
            }
        }
        if (userWrapper.getEmail()!=null && !userWrapper.getEmail().equals("")){
            new UserEditValidator().validateEmailPattern(userWrapper,result);
            User userEmailExist = userService.findByEmail(userWrapper.getEmail());
            if (userEmailExist!=null) {
                UserEditWrapper copyWrapper = new UserEditWrapper(userEmailExist);
                new UserEditValidator().validateEmailExist(copyWrapper, result);
            }
        }
        if (userWrapper.getPassword()!=null && !userWrapper.getPassword().equals("")){
            new UserEditValidator().validateConfirmPassword(userWrapper,result);
        }
        if (result.hasErrors()){
            return "editProfile";
        } else {
            if (userWrapper.getNickname()!=null && !userWrapper.getNickname().equals("")){
                if (userWrapper.getEmail()!=null && !userWrapper.getEmail().equals("")){
                    userService.updateEmail(userWrapper.getEmail());
                }
                if (userWrapper.getPassword()!=null && !userWrapper.getPassword().equals("")){
                    userService.updatePassword(userWrapper.getPassword());
                }
                userService.updateNickname(userWrapper.getNickname());
            } else {
                if (userWrapper.getEmail() != null && !userWrapper.getEmail().equals("")) {
                    userService.updateEmail(userWrapper.getEmail());
                }
                if (userWrapper.getPassword() != null && !userWrapper.getPassword().equals("")) {
                    userService.updatePassword(userWrapper.getPassword());
                }
            }
            return "successEditProfile";
        }
    }
}
