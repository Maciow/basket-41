package pl.basket.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import pl.basket.model.VoteType;
import pl.basket.service.VoteService;

@Controller
public class VoteController {

    private VoteService voteService;

    @Autowired
    public VoteController(VoteService voteService) {
        this.voteService = voteService;
    }

    @GetMapping("/meeting/{id}/vote/{type}")
    public String addVote(@PathVariable Long id, @PathVariable VoteType type){
        voteService.addVote(id,type);
        return "redirect:/games/1";
    }
}
