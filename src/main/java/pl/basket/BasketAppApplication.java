package pl.basket;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
@SpringBootApplication
public class BasketAppApplication {

    public static void main(String[] args) {
        SpringApplication.run(BasketAppApplication.class, args);
    }
}
