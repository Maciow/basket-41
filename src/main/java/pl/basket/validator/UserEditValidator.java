package pl.basket.validator;

import org.springframework.validation.Errors;
import pl.basket.model.User;
import pl.basket.utils.UserUtils;
import pl.basket.wrapper.UserEditWrapper;

public class UserEditValidator {

    public static final String EMAIL_PATTERN = "^[a-zA-z0-9]+[\\._a-zA-Z0-9]*@[a-zA-Z0-9]+{2,}\\.[a-zA-Z]{2,}[\\.a-zA-Z0-9]*$";

    public void validateEmailPattern(UserEditWrapper userWrapper, Errors errors){
        if (userWrapper.getEmail()!=null && !userWrapper.getEmail().equals("")){
            boolean isMatch = UserUtils.checkEmailPattern(EMAIL_PATTERN,userWrapper.getEmail());
            if (!isMatch){
                errors.rejectValue("email","error.emailPattern");
            }
        }
    }

    public void validateConfirmPassword(UserEditWrapper userWrapper, Errors errors){
        String pass = userWrapper.getPassword();
        String confirmPass = userWrapper.getConfirmPassword();

        if (!pass.equals(confirmPass)){
            errors.rejectValue("confirmPassword","error.passwordConfirmIncorrect");
        }
    }

    public void validateEmailExist(UserEditWrapper userWrapper, Errors errors){
        if (userWrapper!=null){
            errors.rejectValue("email","error.userEmailExist");
        }
    }

    public void validateNicknameExist(UserEditWrapper userWrapper, Errors errors){
        if (userWrapper!=null){
            errors.rejectValue("nickname","error.userNickExist");
        }
    }

}
