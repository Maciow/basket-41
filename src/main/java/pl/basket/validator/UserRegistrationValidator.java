package pl.basket.validator;

import org.springframework.validation.Errors;
import pl.basket.model.User;

public class UserRegistrationValidator {

    public void validateConfirmPassword(User user, Errors errors){
        String pass = user.getPassword();
        String confirmPass = user.getConfirmPassword();

        if (!pass.equals(confirmPass)){
            errors.rejectValue("confirmPassword","error.passwordConfirmIncorrect");
        }
    }

    public void validateEmailExist(User user, Errors errors){
        if (user!=null){
            errors.rejectValue("email","error.userEmailExist");
        }
    }

    public void validateNicknameExist(User user, Errors errors){
        if (user!=null){
            errors.rejectValue("nickname","error.userNickExist");
        }
    }
}
