package pl.basket.utils;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class UserUtils {

    public static String getLoggedUserNickname(){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String foundNick = authentication.getName();
        return foundNick;
    }

    public static boolean checkEmailPattern(String pattern, String email){
        Pattern p = Pattern.compile(pattern);
        Matcher m = p.matcher(email);
        return m.matches();
    }
}
