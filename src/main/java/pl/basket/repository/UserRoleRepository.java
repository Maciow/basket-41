package pl.basket.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.basket.model.UserRole;

import java.util.List;

public interface UserRoleRepository extends JpaRepository<UserRole,Long> {
    UserRole findByRole(String role);
    List<UserRole> findAll();
}
