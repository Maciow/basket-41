package pl.basket.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import pl.basket.model.User;
import pl.basket.model.UserState;

import java.util.Optional;

@Repository
@Transactional
public interface UserRepository extends JpaRepository<User,Long> {
    User findByNickname(String nickname);
    User findByEmail(String email);
    Optional<User> findById(Long id);

    @Modifying
    @Query("UPDATE User u SET u.nickname=:newNick WHERE u.nickname=:oldNick")
    void updateNickname(@Param("newNick")String newNick, @Param("oldNick")String oldNick);

    @Modifying
    @Query("UPDATE User u SET u.email=:newEmail WHERE u.nickname=:nickname")
    void updateEmail(@Param("newEmail")String newEmail, @Param("nickname")String nickname);

    @Modifying
    @Query("UPDATE User u SET u.password=:newPassword WHERE u.nickname=:nickname")
    void updatePassword(@Param("newPassword")String newPassword, @Param("nickname")String nickname);

    @Modifying
    @Query(value = "UPDATE user_roles r SET r.roles_id=:roles_id WHERE user_id=:user_id", nativeQuery = true)
    void updateUserRole(@Param("roles_id")Long roles_id, @Param("user_id")Long user_id);

    @Modifying
    @Query("UPDATE User u SET u.state=:state WHERE u.id=:id")
    void updateUserState(@Param("state")UserState state, @Param("id")Long id);
}
