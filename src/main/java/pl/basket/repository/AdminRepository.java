package pl.basket.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import pl.basket.model.User;

@Repository
@Transactional
public interface AdminRepository extends JpaRepository<User, Long> {

    @Query("SELECT u FROM User u ORDER BY u.state DESC,u.nickname")
    Page<User> findAll(Pageable pageable);

    @Query(value = "SELECT * FROM User u WHERE u.nickname LIKE %:word% OR u.id LIKE %:word% OR u.email LIKE %:word% ORDER BY u.state DESC,u.nickname", nativeQuery = true)
    Page<User> findAllSearch(@Param("word")String word, Pageable pageable);

    @Modifying
    @Query("UPDATE User u SET u.password=:newPassword WHERE u.id=:id")
    void resetPassword(@Param("newPassword")String newPassword, @Param("id")Long userId);

    void deleteById(Long userId);
}
