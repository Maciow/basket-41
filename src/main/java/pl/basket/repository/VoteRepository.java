package pl.basket.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import pl.basket.model.Meeting;
import pl.basket.model.User;
import pl.basket.model.Vote;
import pl.basket.model.VoteType;

import java.util.List;

@Repository
@Transactional
public interface VoteRepository extends JpaRepository<Vote,Long> {
    Vote findByMeetingAndUser(Meeting meeting, User user);
    List<Vote> findByMeeting(Meeting meeting);
    void deleteAllByUserId(Long userId);

    @Modifying
    @Query("UPDATE Vote v SET v.voteType=:voteType where v.id=:id")
    void updateVoteType(@Param("voteType")VoteType newType, @Param("id")Long id);

    @Modifying
    @Query("DELETE FROM Vote v WHERE v.meeting=:meeting")
    void deleteVotesFromMeeting(@Param("meeting") Meeting meeting);
}
