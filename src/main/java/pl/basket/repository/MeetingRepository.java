package pl.basket.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import pl.basket.model.Meeting;

import java.util.List;
import java.util.Optional;

@Repository
@Transactional
public interface MeetingRepository extends JpaRepository<Meeting,Long> {

     @Query("SELECT m FROM Meeting m ORDER BY m.timestamp DESC")
     Page<Meeting> findAll(Pageable pageable);
     Optional<Meeting> findById(Long id);
     void deleteAllByUserId(Long userId);

     @Modifying
     @Query("UPDATE Meeting m SET m.plus=:plus WHERE m.id=:id")
     void updatePlusVotes(@Param("plus") int plus, @Param("id") Long meetingId);

     @Modifying
     @Query("UPDATE Meeting m SET m.maybe=:maybe WHERE m.id=:id")
     void updateMaybeVotes(@Param("maybe") int maybe, @Param("id") Long meetingId);

     @Modifying
     @Query("UPDATE Meeting m SET m.minus=:minus WHERE m.id=:id")
     void updateMinusVotes(@Param("minus") int minus, @Param("id") Long meetingId);
}
