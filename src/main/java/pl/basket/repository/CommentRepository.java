package pl.basket.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import pl.basket.model.Comment;

@Repository
@Transactional
public interface CommentRepository extends JpaRepository<Comment,Long> {
    void deleteAllByUserId(Long userId);
}
