package pl.basket.captcha.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestOperations;
import pl.basket.captcha.exceptions.ReCaptchaInvalidException;
import pl.basket.captcha.exceptions.ReCaptchaUnavailableException;
import pl.basket.captcha.response.GoogleResponse;
import pl.basket.captcha.settings.CaptchaSettings;
import pl.basket.captcha.validator.CaptchaValidator;

import javax.servlet.http.HttpServletRequest;
import java.net.URI;
import java.util.regex.Pattern;

@Service
public class CaptchaService implements ICaptchaService {

    private HttpServletRequest request;
    private ReCaptchaAttemptService reCaptchaAttemptService;
    private CaptchaSettings captchaSettings;
    private RestOperations restTemplate;
    private static Pattern RESPONSE_PATTERN = Pattern.compile("[A-Za-z0-9_-]+");

    @Autowired
    public void setReCaptchaAttemptService(ReCaptchaAttemptService reCaptchaAttemptService) {
        this.reCaptchaAttemptService = reCaptchaAttemptService;
    }

    @Autowired
    public void setRequest(HttpServletRequest request) {
        this.request = request;
    }

    @Autowired
    public void setRestTemplate(RestOperations restTemplate) {
        this.restTemplate = restTemplate;
    }

    @Autowired
    public void setCaptchaSettings(CaptchaSettings captchaSettings) {
        this.captchaSettings = captchaSettings;
    }

    @Override
    public void processResponse(String response, Errors errors) {
        int errorCounter = 0;
        if (reCaptchaAttemptService.isBlocked(getClientIP())) {
            new CaptchaValidator().checkCaptcha(response, errors);
            errorCounter++;
            throw new ReCaptchaInvalidException("Client exceeded maximum number of failed attempts");
        }

        if (!responseSanityCheck(response)) {
            if (errorCounter<1) {
                new CaptchaValidator().checkCaptcha(response, errors);
                errorCounter++;
            }
//            throw new ReCaptchaInvalidException("Response contains invalid characters");
        }

        final URI verifyUri = URI.create(String.format("https://www.google.com/recaptcha/api/siteverify?secret=%s&response=%s&remoteip=%s", getReCaptchaSecret(), response, getClientIP()));
        try {
            final GoogleResponse googleResponse = restTemplate.getForObject(verifyUri, GoogleResponse.class);

            if (!googleResponse.isSuccess()) {
                if (googleResponse.hasClientError()) {
                    reCaptchaAttemptService.reCaptchaFailed(getClientIP());
                }
                System.out.println("error nr 3");
                if (errorCounter<1) {
                    new CaptchaValidator().checkCaptcha(response, errors);
                    errorCounter++;
                }
//                throw new ReCaptchaInvalidException("reCaptcha was not successfully validated");
            }
        } catch (RestClientException rce) {
            throw new ReCaptchaUnavailableException("Registration unavailable at this time.  Please try again later.", rce);
        }
        reCaptchaAttemptService.reCaptchaSucceeded(getClientIP());
    }

    private boolean responseSanityCheck(String response){
        return StringUtils.hasLength(response) && RESPONSE_PATTERN.matcher(response).matches();
    }

    @Override
    public String getReCaptchaSite() {
        return captchaSettings.getSite();
    }

    @Override
    public String getReCaptchaSecret() {
        return captchaSettings.getSecret();
    }

    private String getClientIP() {
        final String xfHeader = request.getHeader("X-Forwarded-For");
        if (xfHeader == null) {
            return request.getRemoteAddr();
        }
        return xfHeader.split(",")[0];
    }
}
