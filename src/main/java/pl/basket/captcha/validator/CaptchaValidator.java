package pl.basket.captcha.validator;

import org.springframework.validation.Errors;

public class CaptchaValidator {

    public void checkCaptcha(String response, Errors errors){
        if (response.equals("")){
            errors.reject("error.captcha");
        }
    }
}
